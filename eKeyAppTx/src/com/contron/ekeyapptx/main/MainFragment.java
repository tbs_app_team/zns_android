package com.contron.ekeyapptx.main;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 3.1期新的主页
 */
public class MainFragment extends CommonFragment implements AdapterView.OnItemClickListener, LocationSource,
        AMapLocationListener {

    private BasicActivity mActivity;
    @ViewInject(R.id.main_mapview)
    private MapView mMapView;
    @ViewInject(R.id.user_config_button)
    private ImageView mUserView;
    @ViewInject(R.id.qrscanner_button)
    private ImageView mScannerView;
    @ViewInject(R.id.linearLayout1)
    private LinearLayout mFunctionLayout;
    private AMap aMap;
    private LocationSource.OnLocationChangedListener mListener;
    private AMapLocationClient mlocationClient;
    private AMapLocationClientOption mLocationOption;
    private LatLng latlng;
    private RequestHttp requestHttp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_function_main,
                container, false);
        ViewUtils.inject(this, view);
        mMapView.onCreate(savedInstanceState);
        initMap();

        mUserView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TicketActivity.startActivity(mActivity, TicketActivity.EXTRA_TYPE_PERSONAL_CENTER);
            }
        });
        mScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //权限还没有授予，需要在这里写申请权限的代码
                    ActivityCompat.requestPermissions(mActivity,
                            new String[]{Manifest.permission.CAMERA}, 60);
                } else {
                    DoorLockerActivity.startActivity(mActivity, true);
                }
            }
        });
        mFunctionLayout.addView(super.onCreateView(inflater, container, savedInstanceState));
        return view;
    }

    private void initMap() {
        aMap = mMapView.getMap();
        aMap.setLocationSource(this);
        aMap.setMyLocationEnabled(true);
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        initMapMarks();
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (mListener != null && aMapLocation != null) {
            if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
//                mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
                latlng = new LatLng(aMapLocation.getLatitude(),
                        aMapLocation.getLongitude());

                CameraPosition cp = aMap.getCameraPosition();
                CameraPosition cpNew = CameraPosition.fromLatLngZoom(latlng, cp.zoom);
                aMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cpNew));
                deactivate();
            } else {
                latlng = null;
                String errText = "定位失败," + aMapLocation.getErrorCode() + ": "
                        + aMapLocation.getErrorInfo();
                android.util.Log.e("AmapErr", errText);
            }
        }
    }

    @Override
    public void activate(LocationSource.OnLocationChangedListener onLocationChangedListener) {
        mListener = onLocationChangedListener;
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(mActivity);
            mLocationOption = new AMapLocationClientOption();
            // 设置定位监听
            mlocationClient.setLocationListener(this);
            // 设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            // 设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient.startLocation();
        }
    }

    @Override
    public void deactivate() {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
        }
        mlocationClient = null;
    }

    /**
     * 在地图上添加marker
     */
    private void addMarkersToMap(List<DeviceObject> devices) {

        ArrayList<MarkerOptions> listMarkerOption = new ArrayList<MarkerOptions>();

        for (DeviceObject device : devices) {
            LatLng latlng = new LatLng(device.getLat(), device.getLng());

            if (device.getLat() > 0 && device.getLng() > 0) {

                TextView view = (TextView) View.inflate(mActivity,
                        com.contron.ekeypublic.R.layout.marker_icon, null);
                view.setText("");
                view.setTextColor(Color.BLUE);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                        com.contron.ekeypublic.R.drawable.marker_off);

                MarkerOptions markerOption = new MarkerOptions()
                        .title(device.getName()).position(latlng)
                        .icon(BitmapDescriptorFactory.fromView(view))
                        .draggable(false).snippet(device.getName());

                listMarkerOption.add(markerOption);
            }
        }

        aMap.addMarkers(listMarkerOption, false);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        deactivate();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    protected ContronAdapter<Function> getAdapter(User user) {
        int itemLayoutId = R.layout.activity_function_item;
        List<Function> listData = getListData(user);
        final MainActivity mainActivity = (MainActivity) getActivity();
        mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
                TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
                tvName.setText(item.nameResId);
                tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
                TextView tvtag = viewHolder.findView(R.id.tx_tag);

                switch (item.nameResId) {
                    case R.string.main_tab_item33_name:
                        setTag(tvtag, mainActivity.count);
                        break;
                }
            }
        };
        mGridView.setOnItemClickListener(this);
        return mAdapter;
    }

    public void updateAlertCountTag() {
        mAdapter.notifyDataSetChanged();
    }

    private void setTag(TextView tvtag, int count) {
        if (count != 0) {
            tvtag.setVisibility(View.VISIBLE);

        }else {
            tvtag.setVisibility(View.INVISIBLE);
        }
        tvtag.setText(String.valueOf(count));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int functionId = mAdapter.getItem(position).nameResId;
        switch (functionId) {
            case R.string.main_tab_item40_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_OPEN_DOOR_BY_FSU);
                break;
            case R.string.main_tab_item5_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_5);
                break;
            case R.string.main_tab_item0_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_UNLOCK_BY_KEY);
                break;
            case R.string.main_tab_item16_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_16);
                break;
            case R.string.main_tab_item33_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_33);
                ((MainActivity) getActivity()).count = 0;
                EkeyAPP.getInstance().setWarnCount(0);
                break;
            case R.string.main_tab_item41_name:
                TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_STATION_NAVIGATION);
                break;
        }
    }

    private List<Function> getListData(User user) {
        List<Function> functions = new ArrayList<Function>();
        // 远程开门
        functions.add(new Function(R.drawable.main_remote_control, R.string.main_tab_item40_name));
        // 蓝牙开门
        functions.add(new Function(R.drawable.main_bluetooth_open, R.string.main_tab_item5_name));
        // 钥匙开门
        functions.add(new Function(R.drawable.main_key_open, R.string.main_tab_item0_name));
        // 操作记录
        functions.add(new Function(R.drawable.query, R.string.main_tab_item16_name));
        // 告警查询
        functions.add(new Function(R.drawable.query_alarm, R.string.main_tab_item33_name));
        // 站址导航
        functions.add(new Function(R.drawable.main_navigation, R.string.main_tab_item41_name));

        return functions;
    }

    private void initMapMarks() {
        User user = mActivity.getUser();
        if (user == null)
            return;
        if (!user.isOnline)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        RequestParams params = new RequestParams(user);
        requestHttp.doPost(Globals.MAIN_MAP_INFO, params,
                new RequestHttp.HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (value.contains("success")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(value);
                                        JSONArray jsonArray = jsonObject
                                                .getJSONArray("items");
                                        if (jsonArray.length() == 0) {
                                            return;
                                        } else {
                                            List<DeviceObject> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<DeviceObject>>() {
                                            }.getType());
                                            addMarkersToMap(items);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }
}
