package com.contron.ekeyapptx.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.EkeyAPP.RecLenEventListener;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SettingsFragment;
import com.contron.ekeyapptx.main.OfflineManager.OfflineTimeListener;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.ConfigUtil;

public abstract class CommonFragment extends Fragment implements RecLenEventListener {

	protected GridView mGridView;
	private TextView mOfflineDescText;
	private TextView mOfflineTimeText;

	private ConfigUtil mConfigUtil;
	protected ContronAdapter<Function> mAdapter;
	private User mUser;

	protected boolean mIsOffline = true;// 记录离线任务是否启动

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 注册自动注销事件
		EkeyAPP.getInstance().startTimeTask(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_common, container, false);
		initViews(view);
		initData();
		return view;
	}

	private void initViews(View contentView) {
		mGridView = (GridView) contentView.findViewById(R.id.function_gridView);
		mOfflineDescText = (TextView) contentView.findViewById(R.id.offline_desc_text);
		mOfflineTimeText = (TextView) contentView.findViewById(R.id.offline_time_text);
	}

	private void initData() {
		mUser = EkeyAPP.getInstance().getUser();
		mConfigUtil = ConfigUtil.getInstance(getContext());
		mAdapter = getAdapter(mUser);
		if (null != mAdapter) {
			mGridView.setAdapter(mAdapter);
			adjustGridView();
		}
		initByUser();
	}

	protected abstract  ContronAdapter<Function> getAdapter(User user);

	private void initByUser() {
		if (mUser.isOnline) {
			mOfflineDescText.setVisibility(View.GONE);
			mOfflineTimeText.setVisibility(View.GONE);
		} else {
			mOfflineDescText.setVisibility(View.VISIBLE);
			mOfflineTimeText.setVisibility(View.VISIBLE);
			mIsOffline = offlineIsBegin();
			if (mIsOffline) {
//				mOfflineTimeText.setText(OfflineManager.getInstance().getTotalDelayInfo());
				startCountDown();
			}
			else {
				mOfflineTimeText.setText(OfflineManager.getInstance().getTotalDelayInfo());
				OfflineManager.getInstance().unlock(mUser, mIsOffline);
			}
		}
	}

	protected boolean offlineIsBegin() {
		return DBHelp.getInstance(getContext()).getEkeyDao().isOffineStart(mUser.getUsername());
	}

	protected void startCountDown() {
		if(!DBHelp.getInstance(getContext()).getEkeyDao().isOffineStart(mUser.getUsername()))
			return;
		OfflineManager.getInstance().startOfflineTime(new OfflineTimeListener() {
			@Override
			public void refreshTimeText(String timeText) {
				mOfflineTimeText.setText(timeText);
			}
		});
	}

	@Override
	public void onTimer() {
		// TODO 登陆状态开始更新注册时间
		if (EkeyAPP.getInstance().isLoginStatus()) {
			// 达到自动注销的时间则�?毁所有的界面，跳转到登录界面
			if(null == mConfigUtil)
				mConfigUtil = ConfigUtil.getInstance(getContext());
			if (EkeyAPP.getInstance().getRecLen() == 60
					* Integer.parseInt(mConfigUtil.getString(Globals.KEY_RECLEN_TIME))) {
				EkeyAPP.getInstance().exitRecLen();
			} else
				EkeyAPP.getInstance().updateRecLen();
		}
	}

	/**
	 * 填充适配器
	 * 
	 */
	private void adjustGridView() {
		// 设置主界面模块列表,如果大于3个模块，按照3列显示，否则按照模块数来显示列数
		int funCount = mAdapter.getCount();
		if (funCount >= 3)
			mGridView.setNumColumns(3);
		else
			mGridView.setNumColumns(funCount);

	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdapter != null)
			mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public static class Function {
		public int icoResId;
		public int nameResId;

		public Function(int icoResId, int nameResId) {
			this.icoResId = icoResId;
			this.nameResId = nameResId;
		}
	}


}
