package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SettingsFragment;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeyapptx.plan.PlanActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.ConfigUtil;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class UnlockFragment extends CommonFragment implements OnItemClickListener {
	private User mUser;
	@Override
	protected ContronAdapter<Function> getAdapter(User user) {
		int itemLayoutId = R.layout.activity_function_item;
		mUser = user;
		List<Function> listData = getListData(user);
		mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
				TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
				tvName.setText(item.nameResId);
				tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
			}
		};
		mGridView.setOnItemClickListener(this);
		return mAdapter;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int functionId = mAdapter.getItem(position).nameResId;
		switch (functionId) {
//			case R.string.main_tab_item0_name:
			case R.string.main_tab_item0_off_name:
//				OfflineManager.getInstance().unlock(mUser, mIsOffline);
			{
				if (!mUser.isOnline) {

					if (OfflineManager.getInstance().isOfflineTimeUp()) {
						((BasicActivity)getActivity()).showToast("无离线鉴权信息或者离线鉴权申请的延时已过期，请重新申请！");
						return;
					}
					if(!offlineIsBegin()) {
						OfflineManager.getInstance().updateFixedTicketDelayBeginTime();
						startCountDown();
					}

				}
				String unlockType = ConfigUtil.getInstance(getActivity()).getString(Globals.KEY_UNLOCK_TYPE);
				if (SettingsFragment.UNLOCK_BLUETOOTH.equals(unlockType)) {
					DoorLockerActivity.startActivity(getActivity());
				} else {
					EkeyAPP.getInstance().setBtAddress("");
					TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_UNLOCK_BY_KEY);
				}
			}
				break;
			case R.string.main_tab_item6_name:
				PlanActivity.startActivity(getActivity());
				break;
			case R.string.main_tab_item1_name:
				EkeyAPP.getInstance().setBtAddress("");
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_1);
				break;
			case R.string.main_tab_item18_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_18);
				break;
			case R.string.main_tab_item7_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_7);
				break;
			case R.string.main_tab_item11_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_11);
				break;
			case R.string.main_tab_item13_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_13);
				break;
//			case R.string.main_tab_item20_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_20);
//				break;
//			case R.string.main_tab_item23_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_23);
//				break;
//			case R.string.main_tab_item15_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_15);
//				break;
//			case R.string.main_tab_item37_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_37);
//				break;
		}
	}



	private List<Function> getListData(User user) {
		List<Function> functions = new ArrayList<Function>();
		List<String> arrayList = user.getPermision();
		boolean approvalPermision = false;
		boolean systemPermision = false;

		if (arrayList != null && arrayList.size() > 0) {
			for (String string : arrayList) {
				if (TextUtils.equals(string, "3614"))
					approvalPermision = true;
				else if (TextUtils.equals(string, "0"))
					systemPermision = true;

			}
		}
		if (!user.isOnline) { 
			// 离线鉴权智能钥匙开锁
			functions.add(new Function(
					R.drawable.key1on, R.string.main_tab_item0_off_name));
			return functions;
		}
		// Fsu申请远控解锁
//		functions.add(new Function(R.drawable.station, R.string.main_tab_item15_name));
		// 在线智能钥匙开锁
//		functions.add(new Function(R.drawable.key1on, R.string.main_tab_item0_name));
		// 临时权限鉴权开锁
		functions.add(new Function(R.drawable.jinji, R.string.main_tab_item6_name));
		// 临时权限申请
		functions.add(new Function(R.drawable.temprights, R.string.main_tab_item1_name));
		if (approvalPermision || systemPermision) {
			// 临时权限授权
			functions.add(new Function(R.drawable.tempauth, R.string.main_tab_item18_name));
		}
		// 离线鉴权申请
		functions.add(new Function(R.drawable.delayapply, R.string.main_tab_item7_name));
		// 固定钥匙任务下载
//		functions.add(new Function(R.drawable.preset, R.string.main_tab_item20_name));
		// 借用钥匙登记
		functions.add(new Function(R.drawable.lendkey, R.string.main_tab_item11_name));
		// 归还钥匙登记
		functions.add(new Function(R.drawable.returnkey, R.string.main_tab_item13_name));
		// 远控解锁申请
//		functions.add(new Function(R.drawable.yuancheng_jiesuo, R.string.main_tab_item37_name));
		return functions;
	}

}
