package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.workorder.MyWorkActivity;
import com.contron.ekeyapptx.workorder.WorkBillApplyActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.User;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class WorkFragment extends CommonFragment implements OnItemClickListener{

	@Override
	protected ContronAdapter<Function> getAdapter(User user) {
		int itemLayoutId = R.layout.activity_function_item;
		List<Function> listData = getListData(user);
		mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
				TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
				tvName.setText(item.nameResId);
				tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
			}
		};
		mGridView.setOnItemClickListener(this);
		return mAdapter;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int functionId = mAdapter.getItem(position).nameResId;
		switch (functionId) {
			case R.string.main_tab_item30_name:
				WorkBillApplyActivity.startActivity(getActivity());
				break;
			case R.string.main_tab_item24_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_24);
				break;
			case R.string.main_tab_item25_name:
				MyWorkActivity.startActivity(getActivity());
				break;
			case R.string.main_tab_item26_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_26);
				break;
			case R.string.main_tab_item27_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_27);
				break;
			case R.string.main_tab_item28_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_28);
				break;
			case R.string.main_tab_item29_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_29);
				break;
//			case R.string.main_tab_item38_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_38);
//				break;
		}
	}

	private List<Function> getListData(User user) {
		List<Function> functions = new ArrayList<Function>();
		// 申请工单
		functions.add(new Function(R.drawable.jinzhan_shenpi, R.string.main_tab_item30_name));
		// 派工
		functions.add(new Function(R.drawable.work_bill, R.string.main_tab_item24_name));
		// 添加巡检任务
		functions.add(new Function(R.drawable.work_patrol, R.string.main_tab_item26_name));
		// 我的派工
		functions.add(new Function(R.drawable.work_intime, R.string.main_tab_item25_name));
		// 执行巡检任务
		functions.add(new Function(R.drawable.xunjian_zhixing, R.string.main_tab_item27_name));
		// 到站及时率
		functions.add(new Function(R.drawable.query_workintime, R.string.main_tab_item28_name));
		// 巡检完成率
		functions.add(new Function(R.drawable.query_workpatrol, R.string.main_tab_item29_name));
		// 设备勘查
//		functions.add(new Function(R.drawable.kancha, R.string.main_tab_item38_name));
		return functions;
	}

}
