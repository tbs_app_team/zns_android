package com.contron.ekeyapptx.fsu;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.plan.DeviceActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.DateTimePickerFragment;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 远程解锁申请
 *
 * @author luoyilong
 *
 */
public class DistantApplyFragment extends Fragment {
    private static final int REQUEST_CODE_DEVICE = 100;
    private static final int REQUEST_CODE_HEAD = 101;
    private static final int REQUEST_CODE_IDENTITY = 102;
    @ViewInject(R.id.temporary_apply_et_user_name)
    private EditText etUserName;
    @ViewInject(R.id.temporary_apply_et_mobile)
    private EditText etMobile;
    @ViewInject(R.id.temporary_apply_et_task_name)
    private EditText etTaskName;
    @ViewInject(R.id.temporary_apply_btn_beginTime)
    private Button btnBeginTime;
    @ViewInject(R.id.temporary_apply_btn_endTime)
    private Button btnEndTime;
    @ViewInject(R.id.temporary_apply_btn_device)
    private Button btnDevice;

    private BasicActivity mActivity;
    private ArrayList<Section> Objects = new ArrayList<Section>();
    private ArrayList<Section> resultObject = new ArrayList<Section>();
    SparseArray<ParString> oid = new SparseArray<ParString>();

    private RequestHttp requestHttp;// 后台服务请求
    private BitmapUtils bitmapUtils;// 图片处理对象

    private Calendar beginCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();

    public static DistantApplyFragment newInstance() {
        DistantApplyFragment fragment = new DistantApplyFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();
        getObject();
        EventBusManager.getInstance().register(this);

        bitmapUtils = new BitmapUtils(mActivity).configDefaultBitmapMaxSize(
                BitmapCommonUtils.getScreenSize(mActivity))
                .configDefaultBitmapConfig(Bitmap.Config.RGB_565);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusManager.getInstance().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_distant_apply,
                container, false);
        ViewUtils.inject(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.temporar_apply_title,
                        getString(R.string.main_tab_item37_name)));
        requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        if (!restoreStateFromArguments()) {
            onFirstTimeLaunched();
        }
    }

    /**
     * 设备初始化
     */
    private void onFirstTimeLaunched() {
        //
        EventBusManager.getInstance().post(
                new EventBusType.DownLoadStationLockerDevice());
        User user = EkeyAPP.getInstance().getUser();
        // mPlanApply.setApplyBy(user.getName());
        etUserName.setText(user.getName());
        etMobile.setText(user.getUsername());
//        btnBeginTime.setText(DateUtil.getCurrentDate());
//        btnEndTime.setText(DateUtil.getCurrentDate());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // 保存状态
        saveStateToArguments();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveStateToArguments();
    }

    /**
     * 保存数据
     */
    private void saveStateToArguments() {
        if (getView() != null) {
            Bundle outState = new Bundle();
            // 开始时间结束时间
            outState.putString("beginTime", btnBeginTime.getText().toString());
            outState.putString("endTime", btnEndTime.getText().toString());
            // 区域信息
            outState.putParcelableArrayList("Objects", Objects);

            // 选择设备
            outState.putSparseParcelableArray("oid", oid);

            Bundle arguments = getArguments();
            if (arguments != null)
                arguments.putBundle("internalSavedViewState", outState);
        }
    }

    /**
     * 获取设备
     */
    public void getObject() {
        EkeyDao ekeyDao = DBHelp.getInstance(getContext()).getEkeyDao();
        List<Section> stations = ekeyDao.getSection();
        if (stations == null)
            stations = new ArrayList<Section>();
        Objects.clear();
        Objects.addAll(stations);

    }


    /**
     * 获取保存数据
     *
     * @return
     */
    private boolean restoreStateFromArguments() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            Bundle savedInstanceState = arguments
                    .getBundle("internalSavedViewState");
            if (savedInstanceState != null) {
                // 开始时间结束时间
                btnBeginTime.setText(savedInstanceState.getString("beginTime"));
                btnEndTime.setText(savedInstanceState.getString("endTime"));

                // 区域信息
                oid = savedInstanceState.getSparseParcelableArray("oid");

                Display();// 显示所选择设备
                Objects = savedInstanceState.getParcelableArrayList("Objects");

                return true;
            }
        }
        return false;
    }

    /**
     * ����{@linkplain EventBusManager#post(Object)}
     *
     * @author hupei
     * @date 2015��9��25�� ����11:40:38
     * @param downLoadStationLockerDevice
     */
    public void onEventAsync(EventBusType.DownLoadStationLockerDevice downLoadStationLockerDevice) {
//		List<Section> stations = DBHelp.getInstance(mActivity).getEkeyDao()
//				.getSection();
//
//		if (stations == null)
//			stations = new ArrayList<Section>();
//
//		Objects.clear();
//		Objects.addAll(stations);

    }

    @OnClick({ R.id.temporary_apply_btn_beginTime,
            R.id.temporary_apply_btn_endTime, R.id.temporary_apply_btn_device,
            R.id.temporary_apply_iv_head, R.id.temporary_apply_iv_identity })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.temporary_apply_btn_beginTime:
                DateTimePickerFragment datePickerBegin = DateTimePickerFragment
                        .newInstance(
                                new DateTimePickerFragment.OnDateTimeDoneListener() {

                                    @Override
                                    public void onDateTime(int year,
                                                           int monthOfYear, int dayOfMonth,
                                                           int hourOfDay, int minute,
                                                           String dateTime) {

                                        beginCalendar.set(Calendar.YEAR, year);
                                        beginCalendar.set(Calendar.MONTH,
                                                monthOfYear);
                                        beginCalendar.set(Calendar.DAY_OF_MONTH,
                                                dayOfMonth);
                                        beginCalendar.set(Calendar.HOUR_OF_DAY,
                                                hourOfDay);
                                        beginCalendar.set(Calendar.MINUTE, minute);

                                        btnBeginTime.setText(dateTime);
                                    }
                                }, true);
                datePickerBegin.show(mActivity.getFragmentManager(), "dateStart");
                break;
            case R.id.temporary_apply_btn_endTime:
                DateTimePickerFragment datePickerEnd = DateTimePickerFragment
                        .newInstance(
                                new DateTimePickerFragment.OnDateTimeDoneListener() {

                                    @Override
                                    public void onDateTime(int year,
                                                           int monthOfYear, int dayOfMonth,
                                                           int hourOfDay, int minute,
                                                           String dateTime) {

                                        endCalendar.set(Calendar.YEAR, year);
                                        endCalendar.set(Calendar.MONTH,
                                                monthOfYear);
                                        endCalendar.set(Calendar.DAY_OF_MONTH,
                                                dayOfMonth);
                                        endCalendar.set(Calendar.HOUR_OF_DAY,
                                                hourOfDay);
                                        endCalendar.set(Calendar.MINUTE, minute);

                                        btnEndTime.setText(dateTime);
                                    }
                                }, true);
                datePickerEnd.show(mActivity.getFragmentManager(), "dateEnd");
                break;
            case R.id.temporary_apply_btn_device:
                DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, Objects);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_DEVICE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    setStationsData(bundle);
                }
            }
        }
    }

    /**
     * 保存已选择的设备
     *
     * @param bundle
     */
    private void setStationsData(Bundle bundle) {
        ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
        if (obj == null || obj.size() <= 0) {
            return;
        }
        resultObject.clear();
        resultObject.addAll(obj);
        if (resultObject != null && resultObject.size() > 0) {
            oid.clear();
            for (Section station : resultObject) {
                for (DeviceObject device : station.deviceList) {
                    if (device.isCheck) {

                        ParString parString = new ParString(device.id,
                                device.getName());
                        oid.append(device.id, parString);
                    }
                }
            }
        }
        Display();
    }

    /**
     * 在按钮上显示选择的设备名称
     */
    private void Display() {

        StringBuffer sb = new StringBuffer();
        if (oid != null && oid.size() > 0) {
            for (int i = 0; i < oid.size(); i++) {
                sb.append(oid.valueAt(i).getName() + ",");
            }

            sb.deleteCharAt(sb.length() - 1);
            btnDevice.setText(sb.toString());
        } else {
            btnDevice.setText(R.string.device_title);
        }

    }

    /**
     * 把圖片显示到UI上
     *
     * @param iv
     *            ImageView
     * @param thumbnailPath
     *            文件
     */
    private void displayPhoto(final ImageView iv, final String thumbnailPath) {
        if (TextUtils.isEmpty(thumbnailPath))
            return;

        if (bitmapUtils != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bitmapUtils.display(iv, thumbnailPath);// 显示
                }
            });

        }

    }

    private void onSaveApply() {

        String taskName = etTaskName.getText().toString().trim();
        boolean dateBefore = DateUtil.isDateBefore(beginCalendar, endCalendar);

        if (TextUtils.isEmpty(taskName)) {
            mActivity.showToast("请输入任务名");
            return;
        }

        if (dateBefore) {
            Toast.makeText(getActivity(), "结束日期时间不能小于开始日期时间", Toast.LENGTH_LONG).show();
            return;
        }

        if (oid == null || oid.size() < 1) {
            mActivity.showToast("请选择闭锁对象！");
            return;
        }

        String BeginTime = DateUtil.getStringByFormat(btnBeginTime.getText()
                .toString(), "yyyyMMddHHmmss");
        String EndTime = DateUtil.getStringByFormat(btnEndTime.getText()
                .toString(), "yyyyMMddHHmmss");

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        JSONArray object = new JSONArray();
        try {
            for (int i = 0; i < oid.size(); i++) {
                object.put(i, oid.valueAt(i).getId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.putParams("name", taskName);
        params.putParams("begin_at", BeginTime);
        params.putParams("end_at", EndTime);
        params.putParams("object_id", oid.valueAt(0).getId());

        mActivity.startDialog(R.string.progressDialog_title_submit);
        requestHttp.doPost(Globals.DISTANT_UNLOCK_APPLY, params,
                new RequestHttp.HttpRequestCallBackString() {
                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {// 返回成功
                                    int tid = 0;
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        tid = json.getInt("id");
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    mActivity.showMsgBox("提示", "远程解锁申请提交成功！",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                    mActivity.finish();
                                                }
                                            });

                                } else {// 返回失败
                                    String error = "";
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        error = json.getString("error");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mActivity.showToast(error);

                                }

                            }
                        });

                    }
                });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.finish();
                break;
            case R.id.menu_save:
                onSaveApply();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}