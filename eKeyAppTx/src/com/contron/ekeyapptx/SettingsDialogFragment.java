package com.contron.ekeyapptx;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.util.ConfigUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 悬浮窗式服务器地址参数配置，用于登录界面调用
 * @author BeMysl 2018-08-01
 */
public class SettingsDialogFragment extends DialogFragment implements View.OnClickListener {

    @ViewInject(R.id.settings_dialog_et_ip)
    private EditText mIpAddressEt;
    @ViewInject(R.id.settings_dialog_et_port)
    private EditText mServerPortEt;
    @ViewInject(R.id.settings_dialog_et_socket_port)
    private EditText mSocketPortEt;
    @ViewInject(R.id.settings_dialog_btn_confirm)
    private Button mConfirmBtn;
    @ViewInject(R.id.settings_dialog_btn_cancel)
    private Button mCancelBtn;
    private ConfigUtil mConfigUtil;
    private BasicActivity mActivity;

    public static SettingsDialogFragment newInstance() {
        return new SettingsDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //设置背景透明
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mActivity = (BasicActivity) getActivity();
        mConfigUtil = ConfigUtil.getInstance(mActivity);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_settings_dialog, null);
        ViewUtils.inject(this, view);
        mConfirmBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        initConfig();
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_dialog_btn_confirm:
                if (!checkInput()) {
                    return;
                }
                saveSetting();
                dismiss();
                EkeyAPP.getInstance().exitRecLen();
                break;
            case R.id.settings_dialog_btn_cancel:
                dismiss();
                break;
        }
    }

    /**
     * 初始化界面内容，如果已保存过参数，则填入对应的框中
     */
    private void initConfig() {
        // IP地址
        String ip = mConfigUtil.getString(Globals.KEY_IP_ADDRESS);
        if (!TextUtils.isEmpty(ip)) {
            mIpAddressEt.setText(ip);
        }

        // 服务端口
        String port = mConfigUtil.getString(Globals.KEY_POST);
        if (!TextUtils.isEmpty(port)) {
            mServerPortEt.setText(port);
        }

        // socket端口
        String socketPort = mConfigUtil.getString(Globals.SOCKET_POST);
        if (!TextUtils.isEmpty(socketPort)) {
            mSocketPortEt.setText(socketPort);
        }
    }

    /**
     * 保存参数配置到ConfigUtil中
     */
    private void saveSetting() {

        // web服务
        String serviceAddress = mIpAddressEt.getText().toString() + ":"
                + mServerPortEt.getText().toString();
        // socket服务
        String socketAddress = mIpAddressEt.getText().toString() + ":"
                + mSocketPortEt.getText().toString();

        // 初始WebService地址
        mConfigUtil.setString(Globals.KEY_WSDL_ADDRESS, "http://" + serviceAddress);

        // 访问socket服务地址
        mConfigUtil.setString(Globals.KEY_SOCKET_ADDRESS, "http://" + socketAddress);

        // IP地址
        mConfigUtil.setString(Globals.KEY_IP_ADDRESS, mIpAddressEt.getText().toString());

        // 服务端口
        mConfigUtil.setString(Globals.KEY_POST, mServerPortEt.getText().toString());

        // socket端口
        mConfigUtil.setString(Globals.SOCKET_POST, mSocketPortEt.getText().toString());

        // 初始APP升级版本文件地址
        mConfigUtil.setString(Globals.KEY_VERSION_ADDRESS,
                String.format(Globals.VERSION_URL, serviceAddress));

        // 初始注销时长
        mConfigUtil.setString(Globals.KEY_RECLEN_TIME, "30");
    }

    /**
     * 判断各个输入框中输入的内容是否符合规定
     * @return
     */
    private boolean checkInput() {
        // 判断IP格式
        if (!judgeIP(mIpAddressEt.getText().toString())) {
            mActivity.showToast("ip地址格式不正确,请检查！");
            return false;
        }

        // 判断服务端口范围
        if (!judgePort(mServerPortEt.getText().toString())) {
            mActivity.showToast("服务端口不在正常范围1~65535内!");
            return false;
        }

        // 判断socket端口范围
        if (!judgePort(mSocketPortEt.getText().toString())) {
            mActivity.showToast("推送端口不在正常范围1~65535内!");
            return false;
        }
        return true;
    }

    /**
     * 判断port格式
     */
    private boolean judgePort(String port) {

        if (port.isEmpty()) {
            return false;
        }
        // 第一位不能为0
        if (port.indexOf("0") == 0)
            return false;

        int intPort = Integer.parseInt(port);

        if (intPort > 65535 || intPort < 0) {
            return false;
        }

        return true;
    }

    /**
     * 判断IP格式
     */
    private boolean judgeIP(String ip) {

        if (ip.isEmpty() || !ip.contains(".")) {
            return false;
        }

        int cnt = 0;
        int offset = 0;
        while ((offset = ip.indexOf(".", offset)) != -1) {
            offset = offset + 1;
            cnt++;
        }

        // 包含.个数
        if (cnt != 3)
            return false;

        String[] iparray = ip.split("\\.");

        if (iparray.length != 4)
            return false;

        // 判断按.分割后的数
        for (String splitIp : iparray) {

            if (TextUtils.isEmpty(splitIp))
                return false;

            if (splitIp.length() > 1 && splitIp.indexOf("0") == 0)
                return false;

            int number = Integer.parseInt(splitIp);
            if (number < 0 || number > 255)
                return false;
        }

        return true;
    }
}
