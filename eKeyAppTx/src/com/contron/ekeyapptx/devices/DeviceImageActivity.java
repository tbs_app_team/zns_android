package com.contron.ekeyapptx.devices;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.entities.Exploration;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class DeviceImageActivity extends BasicActivity {

    @ViewInject(R.id.viewpager)
    private ViewPager viewPager;

    @ViewInject(R.id.tx_name)
    private TextView tx_name;

    @ViewInject(R.id.tx_memo)
    private TextView tx_memo;

    @ViewInject(R.id.tx_create_at)
    private TextView tx_create_at;

    @ViewInject(R.id.tx_create_by)
    private TextView tx_create_by;

    private List<View> viewList;

    public static void startActivity(Context context, Parcelable parcelable) {
        Intent intent = new Intent(context, DeviceImageActivity.class);
        intent.putExtra("exploration", parcelable);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_image);
        ViewUtils.inject(this);
        getBasicActionBar().setTitle("设备勘查详情");
        viewList = new ArrayList<View>();
        BitmapUtils bitmapUtils = new BitmapUtils(this);
        Exploration exploration = getIntent().getParcelableExtra("exploration");
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        if (exploration != null && exploration.getFiles() != null) {

            for (int i = 0; i < exploration.getFiles().size(); i++) {
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(layoutParams);
                viewList.add(imageView);
                bitmapUtils.display(imageView, getWld() + exploration.getFiles().get(i));
            }
            tx_name.setText("设备名：" + exploration.getObject_name());
            tx_create_by.setText("创建人：" + exploration.getCreate_by());
            tx_memo.setText("备注：" + exploration.getMemo());
            tx_create_at.setText("创建时间：" + exploration.getCreate_at());
        }
        ImageAdapter adapter = new ImageAdapter();
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class ImageAdapter extends PagerAdapter {

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(viewList.get(position));
            return viewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewList.get(position));
        }

        @Override
        public int getCount() {
            return viewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }
    }

}
