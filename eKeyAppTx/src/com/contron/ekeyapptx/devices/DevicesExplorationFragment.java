package com.contron.ekeyapptx.devices;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Exploration;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 添加巡检任务
 * @author luoyilong
 *
 */
public class DevicesExplorationFragment extends Fragment implements AdapterView.OnItemClickListener {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Exploration> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentItem = 0;// 当前页数
    private int mItemCount = 1;// 总页数
    private User user=null;
    private BitmapUtils bitmapUtils;

    private List<Exploration> taskings = new ArrayList<Exploration>();// 保存查询出来的操作日志

    /**
     * 获取实例
     *
     * @return
     */
    public static DevicesExplorationFragment newInstance() {
        return new DevicesExplorationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
        bitmapUtils = new BitmapUtils(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.public_list, container, false);
        ViewUtils.inject(this, view);

        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

        // 下一页按钮
        loadMoreButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentItem++;
                if (mcurrentItem < mItemCount)// 当前页小于总页数
                {
                    downloadPatrol(mcurrentItem);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item38_name));
        setHasOptionsMenu(true);
        BitmapDisplayConfig displayConfig = new BitmapDisplayConfig();
        displayConfig.setLoadFailedDrawable(getActivity().getResources().getDrawable(R.drawable.cyg));

        mAdapter = new ContronAdapter<Exploration>(mActivity,
                R.layout.fragment_exprolation_item, taskings) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Exploration item, final int position) {

                holder.setText(R.id.tx_name,
                        "设备名：" + item.getObject_name());
                holder.setText(R.id.tx_create_by,
                        "创建人：" + item.getCreate_by());

                holder.setText(R.id.tx_create_at,
                        "创建时间：" + item.getCreate_at());

                holder.setText(R.id.tx_memo,
                        "备注：" + item.getMemo());

                LinearLayout contentLayout = holder.findView(R.id.content_layout);

                for (int i = 0; i < contentLayout.getChildCount(); i++) {
                    ImageView imageView = (ImageView) contentLayout.getChildAt(i);
                    try {
                        imageView.setVisibility(View.VISIBLE);
                        bitmapUtils.display(imageView, mActivity.getWld() + item.getFiles().get(i));
                    } catch (IndexOutOfBoundsException e) {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                }

            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setOnItemClickListener(this);
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item38_name);
        downloadPatrol(0);
    }

    /**
     * 下载巡检任务
     */
    private void downloadPatrol(int currentItem) {

        if (user == null)
            return;

        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        if (currentItem == 0) {// 当前页数为0时清空数据
            mcurrentItem = 0;
            taskings.clear();
            mAdapter.notifyDataSetChanged();
        }

        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(user);
        params.putParams("page", currentItem);
		params.putParams("pagesize", 10);

        requestHttp.doPost(Globals.DEVICE_EXPLORATION, params,
                new HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {
                                    ArrayList<Exploration> listTasking;
                                    try {
                                        JSONObject jsonObject = new JSONObject(
                                                value);
                                        JSONArray jsonArrayTempTask = jsonObject
                                                .getJSONArray("items");
                                        // 获取任务
                                        listTasking = new Gson().fromJson(
                                                jsonArrayTempTask.toString(),
                                                new TypeToken<List<Exploration>>() {
                                                }.getType());

                                        // 获取总页数
                                        if (mcurrentItem == 0) {
                                            mItemCount = jsonObject
                                                    .getInt("pages");
                                        }

                                        if (listTasking != null
                                                && listTasking.size() > 0) {
                                            taskings.addAll(listTasking);
                                            mAdapter.notifyDataSetChanged();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        mActivity.showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }



                            }
                        });

                    }
                });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add, menu);
        // super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.finish();
                break;
            case R.id.menu_add:
                CreateExplorationActivity.startActivity(mActivity);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DeviceImageActivity.startActivity(getActivity(), taskings.get(position));
    }
}

