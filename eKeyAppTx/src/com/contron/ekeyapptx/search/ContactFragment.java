package com.contron.ekeyapptx.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Contact;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.ClearEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ContactFragment extends Fragment {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    @ViewInject(R.id.btn_search)
    private TextView btnSearch;

    @ViewInject(R.id.et_search)
    private ClearEditText etSearch;


    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Contact> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentItem = 0;// 当前页数
    private int mItemCount = 1;// 总页数
    private User user=null;

    private List<Contact> contacts = new ArrayList<Contact>();// 保存查询出来的操作日志

    /**
     * 获取实例
     *
     * @return
     */
    public static ContactFragment newInstance() {
        ContactFragment fragment = new ContactFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        ViewUtils.inject(this, view);

        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

        // 下一页按钮
        loadMoreButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentItem++;
                if (mcurrentItem < mItemCount)// 当前页小于总页数
                {
                    downloadPatrol(mcurrentItem);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });

        btnSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadPatrol(0);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item32_name));
        setHasOptionsMenu(true);

        mAdapter = new ContronAdapter<Contact>(mActivity,
                R.layout.fragment_contact_item, contacts) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Contact item, final int position) {
                holder.setText(R.id.tx_name, item.getName() + " , " + item.getSection() + " , " + item.getMemo());
                holder.setText(R.id.tx_phone, item.getPhone());

            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        downloadPatrol(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item32_name);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadPatrol(int currentItem) {

        if (user == null)
            return;

        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        if (currentItem == 0) {// 当前页数为0时清空数据
            mcurrentItem = 0;
            contacts.clear();
            mAdapter.notifyDataSetChanged();
        }

        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(user);
        params.putParams("page", currentItem);
		params.putParams("search", etSearch.getText().toString());

        requestHttp.doPost(Globals.CONTACT, params,
                new HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {
                                    ArrayList<Contact> listTasking;
                                    try {
                                        JSONObject jsonObject = new JSONObject(
                                                value);
                                        JSONArray jsonArrayTempTask = jsonObject
                                                .getJSONArray("items");
                                        // 获取任务
                                        listTasking = new Gson().fromJson(
                                                jsonArrayTempTask.toString(),
                                                new TypeToken<List<Contact>>() {
                                                }.getType());

                                        // 获取总页数
                                        if (mcurrentItem == 0) {
                                            mItemCount = jsonObject
                                                    .getInt("pages");
                                        }

                                        if (listTasking != null
                                                && listTasking.size() > 0) {
                                            contacts.addAll(listTasking);
                                            mAdapter.notifyDataSetChanged();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        mActivity.showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
