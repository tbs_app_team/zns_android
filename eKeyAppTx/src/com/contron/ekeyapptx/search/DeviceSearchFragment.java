package com.contron.ekeyapptx.search;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.ClearEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DeviceSearchFragment extends Fragment implements AMap.OnMarkerClickListener,
        AdapterView.OnItemClickListener, View.OnKeyListener {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    @ViewInject(R.id.btn_search)
    private TextView btnSearch;

    @ViewInject(R.id.et_search)
    private ClearEditText etSearch;

    @ViewInject(R.id.list_content)
    private View list_content;


    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<DeviceObject> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentItem = 0;// 当前页数
    private int mItemCount = 1;// 总页数
    private User user=null;
    private AMap aMap;
    private MapView mapView;
    private Marker marker;

    private List<DeviceObject> list = new ArrayList<DeviceObject>();// 保存查询出来的操作日志

    ArrayList<DeviceObject> tempList = new ArrayList<DeviceObject>();
    private MenuItem mItem;

    /**
     * 获取实例
     *
     * @return
     */
    public static DeviceSearchFragment newInstance() {
        DeviceSearchFragment fragment = new DeviceSearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_search_map, container, false);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnKeyListener(this);
        ViewUtils.inject(this, view);

        etSearch.setHint("输入设备名");
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState); // 此方法必须重写
        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);


        // 下一页按钮
        loadMoreButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentItem++;
                if (mcurrentItem < mItemCount)// 当前页小于总页数
                {
                    downloadPatrol(mcurrentItem);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });


        btnSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadPatrol(0);
            }
        });
        return view;
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (aMap != null) {
            marker.showInfoWindow();
        }
        return true;
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    private void addMarkersToMap(LatLng latlng, String text, String title, String detail, boolean isStatusOk) {
        marker=aMap.addMarker(new MarkerOptions()
                .position(latlng)
                .title(title)
                .snippet(detail)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromView(getMarkView(text, isStatusOk))));
        marker.showInfoWindow();

    }

    private View getMarkView(String text, boolean isStatusOK) {
        View markView = LayoutInflater.from(getActivity()).inflate(R.layout.mark_view, null, false);
        TextView numText = (TextView) markView.findViewById(R.id.num_text);
        ImageView markerImg = (ImageView) markView.findViewById(R.id.marker_img);
        numText.setText(text);
        if (!isStatusOK) {
            markerImg.setColorFilter(Color.RED);
            numText.setTextColor(Color.WHITE);
        }
        return markView;
    }


    private void addMarkersToMap(LatLng latlng, String text, String title, String detail) {
        marker=aMap.addMarker(new MarkerOptions()
                .position(latlng)
                .title(title)
                .snippet(detail)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.poi_marker_pressed)));
        marker.showInfoWindow();
    }

    /**
     * 初始化AMap对象
     */
    private void init(List<DeviceObject> deviceObjects) {
        aMap = mapView.getMap();
        aMap.setOnMarkerClickListener(this);
        ArrayList<LatLng> latLngs = new ArrayList<LatLng>();
        for (int i = 0; i < deviceObjects.size(); i++) {
            DeviceObject deviceObject = deviceObjects.get(i);
            LatLng latLng = new LatLng(deviceObject.getLat(), deviceObject.getLng());
            latLngs.add(latLng);
            boolean isStatusOK = "正常".equals(deviceObject.getStatus()) &&
                    "正常".equals(deviceObject.getLockstatus());
            addMarkersToMap(latLng, "", deviceObject.getType(), deviceObject.toString(), isStatusOK);
            if (i == (deviceObjects.size() - 1)) {
                aMap.moveCamera(CameraUpdateFactory.changeLatLng(latLng));
            }
        }
    }

    private void init(DeviceObject deviceObject) {
        aMap = mapView.getMap();
        aMap.setOnMarkerClickListener(this);
        ArrayList<LatLng> latLngs = new ArrayList<LatLng>();
        LatLng latLng = new LatLng(deviceObject.getLat(), deviceObject.getLng());
        latLngs.add(latLng);

        boolean isStatusOK = "正常".equals(deviceObject.getStatus()) &&
                "正常".equals(deviceObject.getLockstatus());
        addMarkersToMap(latLng, "", deviceObject.getType(), deviceObject.toString(), isStatusOK);
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(latLng));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item39_name));
        setHasOptionsMenu(true);

        mAdapter = new ContronAdapter<DeviceObject>(mActivity,
                R.layout.fragment_device_search_item, list) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final DeviceObject item, final int position) {
                holder.setText(R.id.tx_devicename,  "设备名：" + item.getName());
                holder.setText(R.id.tx_deviceid,    "设备id：" + item.getId());
                holder.setText(R.id.tx_devicearea,  "所属单位：" + item.getArea());
                holder.setText(R.id.tx_danwei,      "代维单位：" + (TextUtils.isEmpty(item.getAgent_section())?"-":item.getAgent_section()));
                holder.setText(R.id.tx_devicecount, "设备数量：" + item.getLockCount());
                holder.setText(R.id.tx_devicestatus,"设备状态：" + (TextUtils.isEmpty(item.getStatus())?"未知":item.getStatus()));
            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        lisview.setOnItemClickListener(this);
        downloadPatrol(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item39_name);
        mapView.onResume();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.map, menu);
        mItem = menu.findItem(R.id.menu_map);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (list_content.getVisibility() == View.VISIBLE) {
                    mActivity.finish();
                } else {
                    toggleMapView();
                }
                break;
            case R.id.menu_map:
                toggleMapView();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleMapView() {
        if (list_content.getVisibility() == View.VISIBLE) {
            init(list);
            list_content.setVisibility(View.GONE);
            mapView.setVisibility(View.VISIBLE);
            mItem.setTitle("列表显示");
        } else {
            list_content.setVisibility(View.VISIBLE);
            mapView.setVisibility(View.GONE);
            mItem.setTitle("地图显示");
        }
        mItem.setVisible(true);
    }

    private void searchDeviceObject(String searchText) {
        if (TextUtils.isEmpty(searchText)) {
            list.clear();
            list.addAll(tempList);
        } else {
            Iterator<DeviceObject> it = list.iterator();
            while(it.hasNext()){
                DeviceObject deviceObject = it.next();
                if(!deviceObject.getName().contains(searchText)){
                    it.remove();
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void downloadPatrol(int currentItem) {

        if (user == null)
            return;

        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        if (currentItem == 0) {// 当前页数为0时清空数据
            list.clear();
            mAdapter.notifyDataSetChanged();
        }

        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(user);
        params.putParams("page", currentItem);
        params.putParams("search", etSearch.getText().toString());
        String url = String.format(Globals.DEVICE_SEARCH, 1);
        requestHttp.doPost(url, params,
                new HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {
                                    ArrayList<DeviceObject> listTasking;
                                    try {
                                        JSONObject jsonObject = new JSONObject(
                                                value);
                                        JSONArray jsonArrayTempTask = jsonObject
                                                .getJSONArray("items");
                                        // 获取任务
                                        listTasking = new Gson().fromJson(
                                                jsonArrayTempTask.toString(),
                                                new TypeToken<List<DeviceObject>>() {
                                                }.getType());

                                        // 获取总页数
                                        if (mcurrentItem == 0) {
                                            mItemCount = jsonObject
                                                    .getInt("pages");
                                        }

                                        if (listTasking != null
                                                && listTasking.size() > 0) {
                                            list.addAll(listTasking);
                                            tempList.addAll(list);
                                            mAdapter.notifyDataSetChanged();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        mActivity.showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (list_content.getVisibility() == View.VISIBLE) {
            init(list.get(position));
            list_content.setVisibility(View.GONE);
            mapView.setVisibility(View.VISIBLE);
        } else {
            list_content.setVisibility(View.VISIBLE);
            mapView.setVisibility(View.GONE);
        }
        mItem.setVisible(false);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_UP
                && mapView.getVisibility() == View.VISIBLE) {
           toggleMapView();
            return true;
        }
        return false;
    }
}
