package com.contron.ekeyapptx.key;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.BasicBtFragment;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class KeyDetailsFragment extends BasicBtFragment {

    private static final String KEY_NAME = "keyName";
    private static final String KEY_TYPE = "keyType";
    private static final String KEY_SECTION = "keySection";
    private static final String KEY_BTADDR = "keyBtaddr";
    private static final String KEY_OWNER = "keyOwner";
    private static final String KEY_ID = "keyId";

    @ViewInject(R.id.et_key_name)
    private EditText etKeyName;
    @ViewInject(R.id.sp_key_type)
    private Spinner spKeyType;
    @ViewInject(R.id.sp_key_section)
    private Spinner spKeySection;
    @ViewInject(R.id.et_key_btaddr)
    private EditText etKeyBtaddr;
    @ViewInject(R.id.sp_key_owner)
    private Spinner spKeyOwner;
    @ViewInject(R.id.bt_scan_btAddr)
    private Button btScanBtaddr;


    private String mKeyName = "";
    private String mKeyType = "";
    private String mKeySection = "";
    private String mKeyBtaddr = "";
    private String mKeyOwner = "";
    private int mKeyId = -1;

    private ContronAdapter<Dict> mTypeAdapter;
    private List<Dict> types = new ArrayList<Dict>();
    private ContronAdapter<Section> mSectionAdapter;
    private List<Section> sections = new ArrayList<Section>();
    private ContronAdapter<User> mUserAdapter;
    private List<User> users = new ArrayList<User>();

    private RequestHttp requestHttp;
    private BasicActivity mActivity;
    private ContronAdapter<BluetoothDevice> btAdapter;
    private MsgBoxFragment btDeviceDialog;// 蓝牙搜索结果设备框

    private User user;


    public KeyDetailsFragment() {

    }

    public static KeyDetailsFragment newInstance(Smartkey key) {
        KeyDetailsFragment fragment = new KeyDetailsFragment();
        if (key != null) {
            Bundle args = new Bundle();
            args.putString(KEY_NAME, key.getName());
            args.putString(KEY_TYPE, key.getType());
            args.putString(KEY_SECTION, key.getSection());
            args.putString(KEY_BTADDR, key.getBtaddr());
            args.putString(KEY_OWNER, key.getUser());
            args.putInt(KEY_ID, key.getId());
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBeginScan = false;
        mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
        if (getArguments() != null) {
            mKeyName = getArguments().getString(KEY_NAME).trim();
            mKeyType = getArguments().getString(KEY_TYPE).trim();
            mKeySection = getArguments().getString(KEY_SECTION).trim();
            mKeyBtaddr = getArguments().getString(KEY_BTADDR).trim();
            mKeyOwner = getArguments().getString(KEY_OWNER).trim();
            mKeyId = getArguments().getInt(KEY_ID);
        }
        initBluetoothDevice();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_key_details, container, false);
        ViewUtils.inject(this, view);
        btScanBtaddr.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        btAdapter.setEmpty();// 清空适配器
                        if (mBtMsgService != null) {
                            mBtMsgService.disconnect();
                            if (!mBtMsgService.isConnected())
                                mBtMsgService.removeCallbacksConn();
                        }
                        mBtMsgService.getBluetoothAdapter()
                                .startLeScan(leScanCallback);
                        btDeviceDialog.show(
                                mActivity.getFragmentManager(), "btt");
                    }

                });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        //初始化下拉框的适配器
        mTypeAdapter = new ContronAdapter<Dict>(mActivity,
                android.R.layout.simple_spinner_item, types) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Dict item, int position) {
                viewHolder.setText(android.R.id.text1, item.getValue());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                setSpinnerSelectionByValue(spKeyType, mKeyType);
            }
        };
        mTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKeyType.setAdapter(mTypeAdapter);

        mSectionAdapter = new ContronAdapter<Section>(mActivity,
                android.R.layout.simple_spinner_item, sections) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Section item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                setSpinnerSelectionByValue(spKeySection, mKeySection);
            }
        };
        mSectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKeySection.setAdapter(mSectionAdapter);

        mUserAdapter = new ContronAdapter<User>(mActivity,
                android.R.layout.simple_spinner_item, users) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, User item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                setSpinnerSelectionByValue(spKeyOwner, mKeyOwner);
            }
        };
        mUserAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKeyOwner.setAdapter(mUserAdapter);

        etKeyName.setText(mKeyName);
        etKeyBtaddr.setText(mKeyBtaddr);
        initMsgBoxFragment();
        initTypeData();
        initSectionData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.submit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case R.id.menu_submit:
                submit();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 接收BtMsgService发出的状态消息<br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @author hupei
     * @date 2015年9月22日 上午9:36:04
     * @param state
     */
    public void onEventMainThread(BtState state) {
        switch (state) {
            case CONNECTING:// 连接中
                break;
            case DISCONNECTED:// 连接断开
                mActivity.showToast("钥匙连接已断开");
                break;
            case CONNECTED:// 连接成功
                mActivity.showToast("钥匙已连接，请使用钥匙采码");
                // 断开后选择钥匙
                break;
            case CONNECTOUTTIME:// 连接超时
                mActivity.showMsgBox("蓝牙连接超时,请重新连接钥匙!");
                break;
            case DISCOVERED:
                break;
            default:
                break;
        }
    }

    /**
     * 初始化蓝牙钥匙适配器
     */
    private void initBluetoothDevice() {

        btAdapter = new ContronAdapter<BluetoothDevice>(mActivity,
                android.R.layout.simple_list_item_2,
                new ArrayList<BluetoothDevice>()) {

            @Override
            public void setViewValue(ContronViewHolder viewHolder,
                                     BluetoothDevice item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName()).setText(
                        android.R.id.text2, item.getAddress());
            }
        };
    }

    /**
     * 初始化钥匙类型数据
     */
    private void initTypeData() {
        if (mTypeAdapter.getCount() != 0) {
            return;
        }
        List<Dict> smartKeyType = DBHelp.getInstance(mActivity).getEkeyDao()
                .getDicType("sm_type");

        mTypeAdapter.addAllDatas(smartKeyType);
    }

    /**
     * 初始化钥匙所属区域
     */
    private void initSectionData() {
        if (mSectionAdapter.getCount() != 0) {
            return;
        }
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());


        // 请求接口
        String url = String.format(Globals.SECTION_ALL);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        if (value.contains("success")) {
                            List<Section> listSection;
                            listSection = JsonTools.getSections("", value);

                            if (listSection != null
                                    && listSection.size() > 0) {
                                mSectionAdapter.addAllDatas(listSection);
                                initOwnerData();
//                                setEmptyViewShown(false);
                            } else {
//                                setEmptyText("暂无数据！");
//                                setEmptyViewShown(true);
                            }
                        } else {
                            mActivity.showToast("获取区域列表失败");
                        }

                    }
                });

            }
        });
    }

    /**
     * 初始化钥匙拥有者
     */
    private void initOwnerData() {
        if (mUserAdapter.getCount() != 0) {
            return;
        }
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());

        // 请求接口
        String url = String.format(Globals.GET_USER_UNDER);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mActivity.dismissDialog();

                        if (value.contains("success")) {
                            List<User> listUser;
                            // 获取用户列表
                            JSONObject jsonObject;
                            JSONArray jsonArrayuser = null;
                            try {
                                jsonObject = new JSONObject(value);
                                jsonArrayuser = jsonObject.getJSONArray("items");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            listUser = JsonTools.getOwner(jsonArrayuser);

                            if (listUser != null
                                    && listUser.size() > 0) {
                                User emptyUser = new User();
                                listUser.add(0, emptyUser);
                                mUserAdapter.addAllDatas(listUser);
//                                setEmptyViewShown(false);
                            } else {
//                                setEmptyText("暂无数据！");
//                                setEmptyViewShown(true);
                            }
                        } else {
                            mActivity.showToast("获取钥匙拥有者列表失败");
                        }
                    }

                });

            }
        });

    }

    /**
     * 提交新增钥匙或修改钥匙信息
     */
    private void submit() {
        if (!checkData()) {
            return;
        }
        // 提交钥匙信息到服务器
        if (mKeyId == -1) {
            createSmartKey();
        } else {
            updateSmartKey();
        }
    }

    private void createSmartKey() {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("name", mKeyName);
        params.putParams("section", mKeySection);
        params.putParams("type", mKeyType);
        params.putParams("user", mKeyOwner);
        params.putParams("btaddr", mKeyBtaddr);

        // 请求接口
        Section sec = (Section) spKeySection.getSelectedItem();
        String url = String.format(Globals.SMARTKEY_NEW, sec.getId());

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mActivity.dismissDialog();

                        if (value.contains("success")) {
                            mActivity.showToast("新建智能钥匙成功");
                            onBackFragment();
                        } else {
                            try {
                                JSONObject jsonObject = new JSONObject(value);
                                String error = jsonObject.getString("error");
                                mActivity.showToast("新建智能钥匙失败:" + error);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                });

            }
        });
    }

    private void updateSmartKey() {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        Section sec = (Section) spKeySection.getSelectedItem();
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("name", mKeyName);
        params.putParams("section", mKeySection);
        params.putParams("sid", sec.getId());
        params.putParams("type", mKeyType);
        params.putParams("user", mKeyOwner);
        params.putParams("btaddr", mKeyBtaddr);

        // 请求接口

        String url = String.format(Globals.SMARTKEY_UPDATE, mKeyId);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mActivity.dismissDialog();

                        if (value.contains("success")) {
                            mActivity.showToast("修改智能钥匙成功");
                            onBackFragment();
                        } else {
                            mActivity.showToast("修改智能钥匙失败");
                        }
                    }

                });

            }
        });
    }

    private boolean checkData() {
        mKeyName = etKeyName.getText().toString();
        if (TextUtils.isEmpty(mKeyName)) {
            mActivity.showToast("钥匙名称不能为空！");
            return false;
        }
        Dict dict = (Dict) spKeyType.getSelectedItem();
        mKeyType = dict.getValue();
        if (TextUtils.isEmpty(mKeyType)) {
            mActivity.showToast("钥匙类型不能为空！");
            return false;
        }
        Section sec = (Section) spKeySection.getSelectedItem();
        mKeySection = sec.getName();
        if (TextUtils.isEmpty(mKeySection)) {
            mActivity.showToast("所属单位不能为空！");
            return false;
        }
        mKeyBtaddr = etKeyBtaddr.getText().toString();
        if (TextUtils.isEmpty(mKeyBtaddr)) {
            mActivity.showToast("蓝牙地址不能为空！");
            return false;
        }
        User user = (User) spKeyOwner.getSelectedItem();
        mKeyOwner = user.getName();

        return true;
    }

    /**
     * 设置spinner的默认选中
     *
     * @param spinner
     * @param value
     */
    private void setSpinnerSelectionByValue(Spinner spinner, String value) {
        if ("".equals(value)) {
            spinner.setSelection(0);
            return;
        }
        SpinnerAdapter adapter = spinner.getAdapter();
        int positionCount = adapter.getCount();
        for (int i = 0; i < positionCount; i++) {
            if (adapter.getItem(i) instanceof Section) {
                Section section = (Section) adapter.getItem(i);
                if (section.getName().equals(value)) {
                    spinner.setSelection(i);
                    return;
                }
            } else if (adapter.getItem(i) instanceof Dict) {
                Dict dict = (Dict) adapter.getItem(i);
                if (dict.getValue().equals(value)) {
                    spinner.setSelection(i);
                    return;
                }
            } else if (adapter.getItem(i) instanceof User) {
                User user = (User) adapter.getItem(i);
                if (user.getName().equals(value)) {
                    spinner.setSelection(i);
                    return;
                }
            }

        }
    }

    /**
     * 初始化蓝牙钥匙弹出对话框
     */
    private void initMsgBoxFragment() {

        btDeviceDialog = new MsgBoxFragment();
        btDeviceDialog.setCancelable(false);
        btDeviceDialog
                .setNegativeOnClickListener(new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mBtMsgService.getBluetoothAdapter().stopLeScan(
                                leScanCallback);
                        btDeviceDialog.dismiss();
                    }
                });
        btDeviceDialog.setTitle(mActivity.getResources().getString(
                R.string.main_bluetooth_menu));
        btDeviceDialog.setAdapter(btAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String btaddr = btAdapter.getDatas().get(which)
                                .getAddress().replace(":", "");
                        etKeyBtaddr.setText(btaddr);
                        mBtMsgService.getBluetoothAdapter().stopLeScan(
                                leScanCallback);
                        btDeviceDialog.dismiss();
                    }
                });
    }

    // 蓝牙搜索回调方法
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    // 搜索蓝牙结果
//                    String address = device.getAddress().replace(":", "");
//                    // 屏蔽显示智能钥匙
//                    Smartkey btKey = DBHelp.getInstance(mActivity).getEkeyDao()
//                            .findFirstBtKey(address);

                    if (/*btKey == null &&*/ !btAdapter.getDatas().contains(device))
                        btAdapter.addData(device);

                }
            });
        }
    };


    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
