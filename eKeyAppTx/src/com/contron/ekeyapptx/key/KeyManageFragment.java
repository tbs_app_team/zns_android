package com.contron.ekeyapptx.key;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.ClearEditText;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KeyManageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KeyManageFragment extends Fragment {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    @ViewInject(R.id.btn_search)
    private TextView btnSearch;

    @ViewInject(R.id.et_search)
    private ClearEditText etSearch;


    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Smartkey> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentPage = 0;// 当前页数
    private int mPageCount = 1;// 总页数
    private User user = null;// 当前登录用户
    private List<Smartkey> smartkeys = new ArrayList<Smartkey>();// 钥匙

    public static KeyManageFragment newInstance() {
        KeyManageFragment fragment = new KeyManageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        ViewUtils.inject(this, view);

        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

        // 下一页按钮
        loadMoreButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentPage++;
                if (mcurrentPage < mPageCount)// 当前页小于总页数
                {
                    downloadKeys(mcurrentPage);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadKeys(0);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item43_name));

        mAdapter = new ContronAdapter<Smartkey>(mActivity,
                R.layout.fragment_key_info, smartkeys) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Smartkey item, final int position) {
                holder.setText(R.id.tx_key_name, item.getName());
                holder.setText(R.id.tx_key_type, item.getType());
                holder.setText(R.id.tx_key_section, item.getSection());
                holder.setText(R.id.tx_key_btAddr, item.getBtaddr());
                holder.setText(R.id.tx_key_owner, item.getUser());
            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        lisview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Smartkey selectedKey = (Smartkey) parent.getItemAtPosition(position);
                mActivity.repalce(KeyDetailsFragment.newInstance(selectedKey), true);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item43_name);
        downloadKeys(0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.initialize_key, menu);
        inflater.inflate(R.menu.add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case R.id.menu_add:
                mActivity.repalce(KeyDetailsFragment.newInstance(null), true);
                break;
            case R.id.menu_initialize_key:
                InitializeKeyActivity.gotoActivity(mActivity);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadKeys(int currentPage) {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        if (currentPage == 0) {// 当前页数为0时清空数据
            mcurrentPage = 0;
            smartkeys.clear();
            mAdapter.notifyDataSetChanged();
        }
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
        mAdapter.setEmpty();

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("page", currentPage);
        params.putParams("pageize", 20);
        String searchText = etSearch.getText().toString().trim();
        if (!"".equals(searchText)) {
            params.putParams("search", searchText);
        }
        int sid = EkeyAPP.getInstance().getUser().getSid();

        // 请求接口
        String url = String.format(Globals.SECTION_SID_SMARTKEY, sid);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mActivity.dismissDialog();

                        if (value.contains("success")) {
                            List<Smartkey> listSmartkey;
                            // 获取钥匙列表
                            listSmartkey = JsonTools.getSectionKeys(value);

                            if (listSmartkey != null
                                    && listSmartkey.size() > 0) {
                                mAdapter.addAllDatas(listSmartkey);
//                                setEmptyViewShown(false);
                            } else {
//                                setEmptyText("暂无数据！");
//                                setEmptyViewShown(true);
                            }
                        } else {
                            mActivity.showToast("获取智能钥匙列表失败");
                        }
                    }
                });

            }
        });
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }

}
