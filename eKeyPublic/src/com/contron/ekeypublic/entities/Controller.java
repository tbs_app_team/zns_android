package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Controller implements Parcelable {
    private String code; //控制器编码
    private int id; //
    private int oid; //设备id
    private String remark; //备注
    private String type; //类型
    private String work_mode; //工作模式

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWork_mode() {
        return work_mode;
    }

    public void setWork_mode(String work_mode) {
        this.work_mode = work_mode;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeInt(this.id);
        dest.writeInt(this.oid);
        dest.writeString(this.remark);
        dest.writeString(this.type);
        dest.writeString(this.work_mode);
    }

    public Controller() {
    }

    protected Controller(Parcel in) {
        this.code = in.readString();
        this.id = in.readInt();
        this.oid = in.readInt();
        this.remark = in.readString();
        this.type = in.readString();
        this.work_mode = in.readString();
    }

    public static final Parcelable.Creator<Controller> CREATOR = new Parcelable.Creator<Controller>() {
        @Override
        public Controller createFromParcel(Parcel source) {
            return new Controller(source);
        }

        @Override
        public Controller[] newArray(int size) {
            return new Controller[size];
        }
    };
}
