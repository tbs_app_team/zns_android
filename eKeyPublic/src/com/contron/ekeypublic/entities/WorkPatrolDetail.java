package com.contron.ekeypublic.entities;


import com.contron.ekeypublic.util.DateUtil;

public class WorkPatrolDetail {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPatrol_at() {
        return DateUtil.getStringByFormat(patrol_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
    }

    public void setPatrol_at(String patrol_at) {
        this.patrol_at = patrol_at;
    }

    public String getPatrol_by() {
        return patrol_by;
    }

    public void setPatrol_by(String patrol_by) {
        this.patrol_by = patrol_by;
    }

    public DeviceObject getObject() {
        return object;
    }

    public void setObject(DeviceObject object) {
        this.object = object;
    }

    String id;
    String eid;
    String oid;
    String patrol_at;
    String patrol_by;
    DeviceObject object;
}
