package com.contron.ekeypublic.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.contron.ekeypublic.util.DateUtil;

import java.util.List;

public class Exploration implements Parcelable{
    protected Exploration(Parcel in) {
        object_name = in.readString();
        id = in.readString();
        memo = in.readString();
        files = in.createStringArrayList();
        create_by = in.readString();
        create_at = in.readString();
    }

    public static final Creator<Exploration> CREATOR = new Creator<Exploration>() {
        @Override
        public Exploration createFromParcel(Parcel in) {
            return new Exploration(in);
        }

        @Override
        public Exploration[] newArray(int size) {
            return new Exploration[size];
        }
    };

    public String getObject_name() {
        return object_name;
    }

    public void setObject_name(String object_name) {
        this.object_name = object_name;
    }

    String object_name;
    String id;
    String memo;
    List<String> files;
    String create_by;
    String create_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getCreate_at() {
        return DateUtil.getStringByFormat(create_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(object_name);
        dest.writeString(id);
        dest.writeString(memo);
        dest.writeStringList(files);
        dest.writeString(create_by);
        dest.writeString(create_at);
    }
}
