package com.contron.ekeypublic.bluetooth.sc;

import java.util.Random;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.DateUtil;
/**
 * Created by pengx on 2017/11/24.
 */

public class SCSendMsg extends Object {

    private byte[] mLockerId;

    private byte[] mSessionKey;
    private byte[] mInitKey;
    private byte[] mGroupId;
    private byte[] mGroupSecretKey;

    private byte[] mUID;

    private static SCSendMsg instance = null;

    public static SCSendMsg getInstance() {
        if (instance == null) {
            instance = new SCSendMsg();

            instance.mLockerId = new byte[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

            instance.mSessionKey = new byte[]{0, 1, 2, 3, 4, 5, 6, 7};
            instance.mGroupId = new byte[]{0, 0, 0, 1};
            instance.mGroupSecretKey = new byte[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            instance.mInitKey = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

            instance.mUID = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
        }

        return instance;
    }

    public void setLockInfo(byte[] sessionKey, byte[] lockId) {
        mSessionKey = sessionKey;
        mLockerId = lockId;
    }

    public void setMUserId(User user) {
        mUID = intTobytes(user.getId(), 16);
    }

    public byte[] readLockerFrame() {
        byte[] frame = new byte[4];
        frame[0] = (byte) 0x01;
        return frame;
    }

    public byte[] readLockerDetailFrame() {
        byte[] frame = new byte[4];
        frame[0] = (byte) 0x06;
        return frame;
    }

    public byte[] readLockerTypeFrame() {
        byte[] frame = new byte[4];
        frame[0] = (byte) 0x0A;
        return frame;
    }

    public byte[] initLockerFrame() {
        byte[] frame = new byte[40];
        frame[0] = 0x02;
        frame[1] = 0x00;
        frame[2] = 0x24;
        byte[] data = new byte[36];
//		System.arraycopy(mOriginalCode, 0, data, 0, 16);
//		//填入组织ID，暂时用0001代替
//		data[19] = 0x01;
//		//填入组织秘钥，用1111111111111111代替
//		for (int i = 20; i < 36; i++) {
//			data[i] = 0x01;
//		}
        System.arraycopy(mInitKey, 0, data, 0, 16);
        System.arraycopy(mGroupId, 0, data, 16, 4);
        System.arraycopy(mGroupSecretKey, 0, data, 20, 16);
        //使用锁秘钥加密数据
        byte[] temp = new byte[36];
        rc4(mSessionKey, 8, data, 36, temp);
        System.arraycopy(temp, 0, frame, 3, 36);
        byte c = 0;
        for (int i = 0; i < 36; i++) {
            c ^= temp[i];
        }
        frame[39] = c;
        return frame;
    }

    public byte[] openDoorFrame(int lockState) {
        byte[] frame = new byte[67];
        frame[0] = 0x03;
        frame[1] = 0x00;
        frame[2] = 0x3F;
        byte[] data = new byte[63];
        byte[] temp = new byte[63];
        System.arraycopy(mUID, 0, data, 0, 16);
        System.arraycopy(mLockerId, 0, data, 16, 16);
        rc4(mInitKey, 16, data, 32, temp);
        System.arraycopy(temp, 0, data, 0, 32);
        byte[] date = DateUtil.getCurrentDate("yyyyMMddhhmmss").getBytes();
        System.arraycopy(date, 0, data, 32, 14);
        System.arraycopy(mUID, 0, data, 46, 16);
        data[62] = 1;
        if(1 == lockState)
            data[62] = 2;
        //使用锁秘钥加密数据
        rc4(mSessionKey, 8, data, 63, temp);
        System.arraycopy(temp, 0, frame, 3, 63);
        byte c = 0;
        for (int i = 0; i < 63; i++) {
            c ^= temp[i];
        }
        frame[66] = c;
        return frame;
    }

    public byte[] queryBatteryFrame() {
        byte[] frame = new byte[4];
        frame[0] = 0x05;
        return frame;
    }

    public byte[] clearFrame() {
        byte[] frame = new byte[20];
        frame[0] = 0x08;
        frame[1] = 0x00;
        frame[2] = 0x10;
        byte[] code = new byte[16];
        rc4(mGroupSecretKey, 8, mGroupSecretKey, 16, code);
        System.arraycopy(code, 0, frame, 3, 16);
        byte c = 0;
        for (int i = 0; i < 16; i++) {
            c ^= code[i];
        }
        frame[19] = c;
        return frame;
    }

    public byte[] superClearFrame() {
        byte[] frame = new byte[20];
        frame[0] = 0x09;
        frame[1] = 0x00;
        frame[2] = 0x10;
        byte[] temp = new byte[16];
        byte[] code = new byte[16];
        rc4(mSessionKey, 8, temp, 16, code);
        System.arraycopy(code, 0, frame, 3, 16);
        byte c = 0;
        for (int i = 0; i < 16; i++) {
            c ^= code[i];
        }
        frame[19] = c;
        return frame;
    }

    public byte[] modifyLockerId() {
        byte[] frame = new byte[20];
        frame[0] = 0x0B;
        frame[1] = 0x00;
        frame[2] = 0x10;
        byte[] tempId = new byte[4];
        Random randomId = new Random();
        randomId.nextBytes(tempId);
        System.arraycopy(tempId, 0, frame, 3, 4);
        System.arraycopy(mLockerId, 4, frame, 7, 12);
        byte c = 0;
        for (int i = 3; i < 19; i++) {
            c ^= frame[i];
        }
        frame[19] = c;
        return frame;
    }

    public byte[] resetSecretKey() {
        byte[] frame = new byte[36];
        frame[0] = 0x07;
        frame[1] = 0x00;
        frame[2] = 0x20;
        byte[] data = new byte[32];
        System.arraycopy(mGroupSecretKey, 0, data, 0, 16);
        //填入初始秘钥，暂时用0123456789ABCDEF代替
        System.arraycopy(mInitKey, 0, data, 16, 16);
        //使用组织秘钥加密数据
        byte[] temp = new byte[32];
        rc4(mGroupSecretKey, 16, data, 32, temp);
        System.arraycopy(temp, 0, frame, 3, 32);
        byte c = 0;
        for (int i = 0; i < 32; i++) {
            c ^= temp[i];
        }
        frame[35] = c;
        return frame;
    }

    //校验函数
    public void rc4(byte[] pSecret, int secretLen, byte[] pMessage, int messageLen, byte[] pOut) {
        int[] iS = new int[256];
        byte[] iK = new byte[256];
        int i, j, x, t, iY, temp;
        byte iCY;
        for (i = 0; i < 256; i++) {
            iS[i] = i;
        }
        for (i = 0; i < 256; i++) {
            iK[i] = pSecret[(i % secretLen)];
        }
        j = 0;
        for (i = 0; i < 256; i++) {
            j = (j + iS[i] & 0xFF + iK[i] & 0xFF) % 256;
            temp = iS[i];
            iS[i] = iS[j];
            iS[j] = temp;
        }
        i = 0;
        j = 0;
        for (x = 0; x < messageLen; x++) {
            i = (i + 1) % 256;
            j = (j + iS[i] & 0xFF) % 256;
            temp = iS[i] & 0xFF;
            iS[i] = iS[j];
            iS[j] = temp;
            t = (iS[i] + (iS[j] & 0xFF % 256)) % 256;
            iY = iS[t] & 0xFF;
            iCY = (byte) iY;
            pOut[x] = (byte) (pMessage[x] ^ iCY);
        }
    }

    private byte[] intTobytes(int mInt, int length) {
        byte[] mBytes = new byte[length];
        if (mInt != 0) {

            String uHex = BtMsgConver.toHexString(mInt);
            byte[] temp = BtMsgConver.hexStrToByteLittle(uHex);

            System.arraycopy(temp, 0, mBytes, length-temp.length, temp.length);
        }
        return mBytes;
    }

}
