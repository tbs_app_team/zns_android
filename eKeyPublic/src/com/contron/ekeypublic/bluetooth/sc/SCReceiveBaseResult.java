package com.contron.ekeypublic.bluetooth.sc;

/**
 * Created by pengx on 2017/11/28.
 */

public class SCReceiveBaseResult extends Object {

    private boolean mState;

    public boolean ismState() {
        return mState;
    }

    public void setmState(boolean mState) {
        this.mState = mState;
    }
}
