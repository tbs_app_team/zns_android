package com.contron.ekeypublic.bluetooth.sc;

import com.contron.ekeypublic.bluetooth.BtMsgConver;

/**
 * Created by pengx on 2017/11/24.
 */



public class SCReceiveMsg extends Object {
    private int recvType;
    private byte[] mData;

    public SCReceiveMsg(byte[] mData) {

        this.mData = mData;
        this.recvType = mData[0];
    }

    public int getRecvType() {
        return recvType;
    }

    public SCReceiveBaseResult parseLockInfoMsg () {
        SCReceiveBaseResult result = new SCReceiveBaseResult();

        if (this.recvType == 0x01) {

            if (this.mData[2] == 30) {

                result.setmState(true);

                int mLockerState = 0;
                int mLockerInstallState = 0;

                byte[] mLockerId = new byte[16];
                byte[] mSessionKey = new byte[8];

                System.arraycopy(mData, 24, mSessionKey, 0, 8);
                System.arraycopy(mData, 3, mLockerId, 0, 16);

                mLockerState = mData[19] >> 4;
                mLockerInstallState = mData[32];

                SCLockInfo.getInstance().setLockInfo(mLockerState, mLockerInstallState, mLockerId, mSessionKey);
                SCSendMsg.getInstance().setLockInfo(mSessionKey, mLockerId);
            }
        }

        return result;
    }

    public SCReceiveBaseResult parseInitMsg () {
        SCReceiveBaseResult result = new SCReceiveBaseResult();

        if (this.recvType == 0x02) {

            result.setmState(this.mData[3] == 1);
            if (result.ismState()) {
                SCLockInfo.getInstance().setmLockInstallState(1);
            }
        }

        return result;
    }

    public SCReceiveBaseResult parseSwitchMsg () {
        SCReceiveBaseResult result = new SCReceiveBaseResult();

        if (this.recvType == 0x03) {

            result.setmState(this.mData[3] == 1);
            if (result.ismState()) {
                SCLockInfo.getInstance().changemLockerState();
            }
        }

        return result;
    }

    public SCBatteryResult parseBattery () {
        SCBatteryResult result = new SCBatteryResult();

        if (this.recvType == 0x05) {

            if (this.mData[2] == 4) {

                result.setmState(true);

                byte[] keyType = new byte[2];
                System.arraycopy(mData, 5, keyType, 0, 2);

                result.setmKeyEnergy(mData[3]);
                result.setmKeyMan(mData[4]);
                result.setmKeyType(Integer.parseInt(BtMsgConver.bytesToHexStr(keyType)));

            }
        }

        return result;
    }

    public SCReceiveBaseResult parseClearMsg () {
        SCReceiveBaseResult result = new SCReceiveBaseResult();

        if (this.recvType == 0x08) {

            byte[] ret = new byte[1];
            byte[] ret2 = new byte[1];
            ret2[0] = this.mData[3];
            SCSendMsg.getInstance().rc4(SCLockInfo.getInstance().getmSessionKey(), 8, ret2, 1, ret);

            result.setmState(ret[0] == 1);
            if (result.ismState()) {
                SCLockInfo.getInstance().setmLockInstallState(0);
            }
        }

        return result;
    }

    public SCReceiveBaseResult parseSuperClearMsg () {
        SCReceiveBaseResult result = new SCReceiveBaseResult();

        if (this.recvType == 0x09) {

            byte[] ret = new byte[1];
            byte[] ret2 = new byte[1];
            ret2[0] = this.mData[3];
            SCSendMsg.getInstance().rc4(SCLockInfo.getInstance().getmSessionKey(), 8, ret2, 1, ret);

            result.setmState(ret[0] == 1);
            if (result.ismState()) {
                SCLockInfo.getInstance().setmLockInstallState(0);
            }
        }

        return result;
    }
}

/**
class SCLockTypeResult extends SCReceiveBaseResult {
    private String mLockType;

    public String getmLockType() {
        return mLockType;
    }

    public void setmLockType(String mLockType) {
        this.mLockType = mLockType;
    }
}

class SCLockDetailResult extends SCReceiveBaseResult {
    private String mLockId;
    private int mCorpId;
    private int mHardId;
    private int mSoftId;

    public String getmLockId() {
        return mLockId;
    }

    public void setmLockId(String mLockId) {
        this.mLockId = mLockId;
    }

    public int getmCorpId() {
        return mCorpId;
    }

    public void setmCorpId(int mCorpId) {
        this.mCorpId = mCorpId;
    }

    public int getmHardId() {
        return mHardId;
    }

    public void setmHardId(int mHardId) {
        this.mHardId = mHardId;
    }

    public int getmSoftId() {
        return mSoftId;
    }

    public void setmSoftId(int mSoftId) {
        this.mSoftId = mSoftId;
    }
}

class SCLockLogResult extends SCReceiveBaseResult {
    private String mDate;
    private String mUID;
    private int mType;
    private int mResult;

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmUID() {
        return mUID;
    }

    public void setmUID(String mUID) {
        this.mUID = mUID;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public int getmResult() {
        return mResult;
    }

    public void setmResult(int mResult) {
        this.mResult = mResult;
    }
}
 **/

